public class ProductOfArrayExceptSelf {
    public static void main(String[] args) {

        int[] nums = {1,2,3,4};

        productExceptSelf(nums);
    }

    static int[] productExceptSelf(int[] nums) {
        int[] product = new int[nums.length];

        for(int i = 0; i < nums.length; i++) {
            product[i] = 1;
            for(int j = 0; j < nums.length; j++) {
                if (j != i) {
                    product[i] *= nums[j];
                }
            }
        }

        return product;
    }
}
