public class JumpGame {
    public static void main(String[] args) {

        int[] nums = {2,3,1,1,4};
        System.out.println(canJump(nums));

    }

    static boolean canJump(int[] nums) {
        boolean[] isReachable = new boolean[nums.length];

        isReachable[0] = true;

        for (int i = 0; i < nums.length; i++) {

            if (isReachable [i] && nums [i] > 0) {
                int maxJumps = nums[i];

                for (int j = 1; j <= maxJumps; j++) {
                    if(i + j < isReachable.length)
                    {
                        isReachable[i+j] =  true;
                    }
                }
            }
        }

        return isReachable [nums.length-1];
    }
}

