public class MaximumSubarray {
    public static void main(String[] args) {
        int[] nums = {-2,1,-3,4,-1,2,1,-5,4};

        int maxSum = maxSubArray(nums);

        System.out.println(maxSum);
    }

    static int maxSubArray(int[] nums) {

        int maxSum = Integer.MIN_VALUE;
        int currentMax = 0;

        for (int i = 0; i < nums.length; i++)
        {
            currentMax = currentMax + nums[i];
            if (maxSum < currentMax) {
                maxSum = currentMax;
            }
            if (currentMax < 0) {
                currentMax = 0;
            }
        }
        return maxSum;
    }
}
