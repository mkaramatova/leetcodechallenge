import java.util.Arrays;
import java.util.HashMap;

public class SingleNumberBinarySearch {
    public static void main(String[] args) {
        int[] nums = {4,1,2,1,2};

        Arrays.sort(nums);

        binarySearch(nums, 0, nums.length-1);
    }

    public static void binarySearch(int[] arr, int low, int high)
    {
        if(low > high) {
            return;
        }
        if(low == high) {
            System.out.println(arr[low]);
            return;
        }

        int mid = (low + high)/2;

        if(mid % 2 == 0) {
            if(arr[mid] == arr[mid+1]) {
                binarySearch(arr, mid + 2, high);
            }
            else {
                binarySearch(arr, low, mid);
            }
        }
        else if(mid % 2 == 1)
        {
            if(arr[mid] == arr[mid-1]) {
                binarySearch(arr, mid + 1, high);
            }
            else {
                binarySearch(arr, low, mid - 1);
            }
        }
    }
}
