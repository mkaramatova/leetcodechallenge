public class BuyAndSell {

    public static void main(String[] args) {
        int[] prices = {7,1,5,3,6,4};

        System.out.println(maxProfit(prices));
    }

    static int maxProfit(int[] prices) {
        int maxProfit = -1;
        int buyPrice = 0;
        int sellPrice = 0;

        boolean buyIndex = true;

        for (int i = 0; i < prices.length-1; i++) {

            sellPrice = prices[i+1];

            if (buyIndex) {
                buyPrice = prices[i];
            }

            if (sellPrice < buyPrice) {
                buyIndex = true;
                continue;
            }

            else {
                int currentProfit = sellPrice - buyPrice;
                if (currentProfit > maxProfit) {
                    maxProfit = currentProfit;
                }
                buyIndex = false;
            }

        }

        return maxProfit;
    }
}
