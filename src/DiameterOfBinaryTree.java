public class DiameterOfBinaryTree {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }
    public int diameterOfBinaryTree(TreeNode root) {

        if (root == null) {
            return 0;
        }

        int left = diameterOfBinaryTree(root.left);
        int right = diameterOfBinaryTree(root.right);

        int diameter = depth(root.left) + depth(root.right);

        return Math.max(diameter, Math.max(left, right));
    }

    private int depth(TreeNode root) {
        return root == null ? 0 : 1  + Math.max(depth(root.left), depth(root.right));
    }
}
