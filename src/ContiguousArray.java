public class ContiguousArray {

    public static void main(String[] args) {

        int[] nums = {0,1};

        System.out.println(findMaxLength(nums));
    }

    static int findMaxLength(int[] nums) {
        int maxLen = 0;
        int currentLen = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                nums[i] = -1;
            }
        }

        int sum = 0;

        for (int i = 0; i < nums.length - 1; i+=2) {
            sum += nums[i] + nums[i + 1];
            currentLen+=2;

            if(sum != 0) {
                if (currentLen > maxLen) {
                    maxLen = currentLen;
                }
                currentLen = 0;
                sum = 0;
            }

            if (i == nums.length - 2 && nums.length % 2 != 0) {
                if(sum + nums[i + 2] == 0) {
                    currentLen += 1;
                }

                if (currentLen > maxLen) {
                    maxLen = currentLen;
                }
            } else if (i == nums.length - 2 && nums.length % 2 == 0) {

                    if (currentLen > maxLen) {
                        maxLen = currentLen;
                    }
            }
        }

        return maxLen;
    }
}
