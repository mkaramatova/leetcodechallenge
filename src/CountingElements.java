import java.util.Arrays;
import java.util.HashSet;

public class CountingElements {
    public static void main(String[] args) {
        int[] arr = {1,1,2,2};

        System.out.println(countElements(arr));
    }

    static int countElements(int[] arr) {
        int count = 0;

        HashSet<Integer> nums = new HashSet();

        for (int n : arr)
        {
            nums.add(n);
        }

        for (int i = 0; i < arr.length; i++)
        {
            if (nums.contains(arr[i] + 1)) {
                count++;
            }
        }


        return count;
    }
}