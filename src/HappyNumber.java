import java.util.HashSet;

public class HappyNumber {

    public static void main(String[] args) {
        int n = 19;

        System.out.println(isHappy(n));
    }

    static int findSumOfSquares(int n)
    {
        int squareSum = 0;
        while (n > 0)
        {
            int digit = n % 10;
            squareSum += digit * digit;
            n = n / 10;
        }
        return squareSum;
    }

    static boolean isHappy(int n)
    {

        HashSet<Integer> s = new HashSet<Integer>();
        s.add(n);

        while (true)
        {

            if (n == 1) {
                return true;
            }

            n = findSumOfSquares(n);

            if ((s.contains(n) && n != (int)s.toArray()[ s.size()-1 ] )) {
                return false;
            }

            s.add(n);
        }
    }
}
