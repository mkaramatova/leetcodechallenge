import java.util.Stack;

public class ValidParenthesisString {
    public static void main(String[] args) {
        System.out.println(checkValidString("(*))") );
    }

    static boolean checkValidString(String s) {
        if(s == "") {
            return true;
        }
        else {

            Stack<Character> st = new Stack<>();

            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '*') {
                    if (st.isEmpty()) {
                        st.push(s.charAt(i));
                    }
                    else {
                        if (st.peek() == '(' || st.peek() == '*') {
                            st.pop();
                        }
                    }
                }
                else if (s.charAt(i) == '(') {
                    st.push(s.charAt(i));
                }
                else if (s.charAt(i) == ')') {
                    if (st.isEmpty()) {
                        return false;
                    }
                    else if (!st.isEmpty()) {
                        if (st.peek() == '(' || st.peek() == '*') {
                            st.pop();
                        }
                    }
                }
            }
            if (st.isEmpty() || st.peek() == '*') {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
