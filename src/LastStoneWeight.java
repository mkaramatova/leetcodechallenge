import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class LastStoneWeight {
    public static void main(String[] args) {
        int[] stones = {2,7,4,1,8,1};
        System.out.println(lastStoneWeight(stones));
    }

    static int lastStoneWeight(int[] stones) {
        Arrays.sort(stones);

        List<Integer> stonesList = Arrays.stream(stones).boxed().collect(Collectors.toList());

        while (stonesList.size() > 1) {
            if (stonesList.get(stonesList.size() - 1) == stonesList.get(stonesList.size() - 2)) {
                stonesList.remove(stonesList.size() - 1);
                stonesList.remove(stonesList.size() - 1);
            }
            else {
                int newStone = stonesList.get(stonesList.size() - 1) - stonesList.get(stonesList.size() - 2);
                stonesList.remove(stonesList.size() - 1);
                stonesList.remove(stonesList.size() - 1);
                stonesList.add(newStone);
                Collections.sort(stonesList);
            }
        }

        if (stonesList.size() == 1) {
            return stonesList.get(0);
        }
        else {
            return 0;
        }
    }
}
