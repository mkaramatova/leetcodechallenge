public class MiddleOfLinkedList {
    public static void main(String[] args) {

        // method works in leetcode
    }

    static ListNode middleNode(ListNode head) {

        int size = 0;

        for (ListNode n = head; n != null; n = n.next) {
            size++;
        }

        for (int i = 1; i <= (size/2); i++) {
            head = head.next;
        }

        return head;

    }
}
