import static java.lang.Math.max;

public class LongestCommonSubsequence {
    public static void main(String[] args) {
        System.out.println(longestCommonSubsequence("abcde", "ace"));
    }

    static int longestCommonSubsequence(String text1, String text2) {
        int current = 0;
        int max = 0;

        int n = text1.length();
        int m = text2.length();
        String longer = text1;
        String shorter = text2;

        if (text1.length() < text2.length()) {
            n = text2.length();
            m = text1.length();
            longer = text2;
            shorter = text1;
        }

        int[][] DP = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {

                if (i == 0 || j == 0) {
                    if (shorter.charAt(i) == longer.charAt(j)) {
                        DP[i][j] = 1;
                        current = 1;
                    }
                    else {
                        DP[i][j] = current;
                    }
                }
                else {

                    if (shorter.charAt(i) == longer.charAt(j)) {
                        DP[i][j] = DP[i - 1][j - 1] + 1;
                        current = DP[i][j];
                    }

                    else {
                        DP[i][j] = current;
                    }
                }

                if (current > max) {
                    max = current;
                }
            }
        }

        return max;
    }
}
