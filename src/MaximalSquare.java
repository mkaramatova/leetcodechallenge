public class MaximalSquare {
    public static void main(String[] args) {
        char matrix[][] = {{1, 0, 1, 0, 0},
                {1, 0, 1, 1, 1},
                {1, 1, 1, 1, 1},
                {1, 0, 0, 1, 0}};

        System.out.println(maximalSquare(matrix));
    }

    static int maximalSquare(char[][] matrix) {
        int maxDimension = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] == '1') {
                    int dimension = getMaxSquareDimension(matrix, i, j);
                    maxDimension = Math.max(maxDimension, dimension);
                }
            }
        }

        return maxDimension * maxDimension;
    }

    static int getMaxSquareDimension(char[][] matrix, int r, int c) {
        int dim = 2;

        while (true) {
            if (isSquareOfGivenDimension(dim, r, c, matrix)) dim++;
            else break;
        }

        return dim - 1;
    }

    static boolean isSquareOfGivenDimension(int dim, int r, int c, char[][] matrix) {
        int rows = matrix.length, columns = matrix[0].length;
        int maxRow = r + dim - 1, maxColumn = c + dim - 1;
        if (maxRow >= rows || maxColumn >= columns) return false;

        for (int i = r; i <= maxRow; i++) {
            if (matrix[i][maxColumn] == '0') return false;
        }

        for (int j = c; j <= maxColumn - 1; j++) {
            if (matrix[maxRow][j] == '0') return false;
        }

        return true;
    }
}
