
import java.util.Arrays;

public class MoveZeroes {
    public static void main(String[] args) {
        int[] nums = {0,1,0,3,12};

        moveZeroes(nums);
    }

    static void moveZeroes(int[] nums) {

        int nonZeroes = 0;

        for (int i = 0; i < nums.length; i++) {
            if(nums[i] != 0) {
                nums[nonZeroes++] = nums[i];
            }
        }

        while(nonZeroes < nums.length) {

            nums[nonZeroes++] = 0;

        }

        System.out.println(Arrays.toString(nums));
    }
}
