public class NumberOfIslands {

    public static void main(String[] args) {
        char grid[][] = {{1, 1, 1, 1, 0},
                {1, 1, 0, 1, 0},
                {1, 1, 0, 0, 0},
                {0, 0, 0, 0, 0}};

        System.out.println(numIslands(grid));
    }

    static int numIslands(char[][] grid) {
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {
                    check(i, j, grid);
                    count++;
                }
            }
        }
        return count;
    }

    static void check(int start, int end, char[][] grid) {
        if (start < 0 || end < 0 || start >= grid.length || end >= grid[0].length || grid[start][end] == '0') {
            return;
        }

        grid[start][end] = '0';
        check(start + 1, end, grid);
        check(start - 1, end, grid);
        check(start, end + 1, grid);
        check(start, end - 1, grid);
    }
}
