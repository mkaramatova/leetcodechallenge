public class PerformStringShifts {
    public String stringShift(String s, int[][] shift) {
        int i, amount = 0;

        int len = s.length();

        String sub1 = "", sub2 = "";

        for (i = 0; i < shift.length; i++) {
            if (shift[i][0] == 0) {
                amount = shift[i][1];
                sub1 = s.substring(0, amount);
                sub2 = s.substring(amount);
                s = sub2 + sub1;
            } else {
                amount = shift[i][1];
                sub1 = s.substring(len - amount);
                sub2 = s.substring(0, len - amount);
                s = sub1 + sub2;
            }
        }

        return s;

    }
}
