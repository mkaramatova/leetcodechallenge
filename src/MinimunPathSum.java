public class MinimunPathSum {

    public static void main(String[] args) {
        int grid[][] = {{1, 3, 1},
                {1, 5, 1},
                {4, 2, 1}};

        System.out.println(minPathSum(grid));
    }

    static int minPathSum(int[][] grid) {
        int[][] minTotalSum = new int[grid.length][grid[0].length];

        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[row].length; col++) {
                if (row == 0) {
                    if (col == 0) {
                        minTotalSum[row][col] = grid[row][col];
                    } else {
                        minTotalSum[row][col] = minTotalSum[row][col - 1] + grid[row][col];
                    }
                } else {
                    if (col == 0) {
                        minTotalSum[row][col] = minTotalSum[row - 1][col] + grid[row][col];
                    } else {
                        int minimumSum = Math.min(minTotalSum[row - 1][col], minTotalSum[row][col - 1]);
                        minTotalSum[row][col] = minimumSum + grid[row][col];
                    }
                }
            }
        }
        return minTotalSum[grid.length - 1][grid[0].length - 1];
    }
}
