import java.util.Stack;

public class BackSpaceStringCompare {
    public static void main(String[] args) {

        System.out.println(backspaceCompare("ab#c", "ad#c"));
        //System.out.println(backspaceCompare("c#d#", "ab##"));
    }

    static boolean backspaceCompare(String S, String T) {
        return removeEmptySpaces(S).equals(removeEmptySpaces(T));
    }

    static String removeEmptySpaces (String str) {

        Stack st = new Stack();

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '#') {
                if (!st.isEmpty()) {
                    st.pop();
                }
            }
            else {
                st.push(str.charAt(i));
            }
        }

        return st.toString();
    }
}
