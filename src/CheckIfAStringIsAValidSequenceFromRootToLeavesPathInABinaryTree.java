public class CheckIfAStringIsAValidSequenceFromRootToLeavesPathInABinaryTree {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public boolean isValidSequence(TreeNode root, int[] arr) {
        return helper(root, arr, 0);
    }

    private boolean helper(TreeNode root, int[] arr, int idx) {
        if (root == null) return false;
        if (arr[idx] != root.val) return false;
        if (idx + 1 == arr.length) return root.left == null && root.right == null;
        return helper(root.left, arr, idx + 1) || helper(root.right, arr, idx + 1);
    }
}
