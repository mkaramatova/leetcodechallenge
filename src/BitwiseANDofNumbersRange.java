public class BitwiseANDofNumbersRange {
    public static void main(String[] args) {
        System.out.println(rangeBitwiseAnd(5, 7));
    }

    static int rangeBitwiseAnd(int m, int n) {
        int bitwiseAND = m;

        for (int i = m + 1; i <= n; i++ ) {
            bitwiseAND = bitwiseAND&i;
        }

        return bitwiseAND;
    }
}
