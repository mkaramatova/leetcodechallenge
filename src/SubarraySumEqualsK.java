import java.util.HashMap;

public class SubarraySumEqualsK {
    public static void main(String[] args) {
        int[] nums = {1,1,1};
        int k = 2;

        System.out.println(subarraySum(nums, k));
    }

    static int subarraySum(int[] nums, int k) {
        int numOfSubarrays = 0;

        HashMap <Integer, Integer> sums = new HashMap<>();

        int currentSum = 0;

        for (int i = 0; i < nums.length; i++) {
            currentSum += nums[i];

            if(currentSum == k) {
                numOfSubarrays++;
            }

            if (sums.containsKey(currentSum - k)) {
                numOfSubarrays += sums.get(currentSum - k);
            }

            sums.put(currentSum, sums.getOrDefault(currentSum, 0) + 1);

        }

        return numOfSubarrays;
    }
}
