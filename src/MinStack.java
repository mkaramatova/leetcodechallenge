import java.util.Stack;

class Node {
    int val;
    int min;

    public Node(int val, int min) {
        this.val = val;
        this.min = min;
    }
}

class MinStack {

    Stack<Node> stack;

    public MinStack() {
        stack = new Stack<>();
    }

    public void push(int x) {
        stack.push(new Node(x, stack.size() == 0 ? x : Math.min(x, getMin())));
    }

    public void pop() {
        stack.pop();
    }

    public int top() {
        return stack.peek().val;
    }

    public int getMin() {
        return stack.peek().min;
    }
}
