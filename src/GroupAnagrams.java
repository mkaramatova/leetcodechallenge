import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class GroupAnagrams {
    public static void main(String[] args) {

        //String[] strs =  {"eat", "tea", "tan", "ate", "nat", "bat"};

        String[] strs =  {"", ""};

        System.out.println(groupAnagrams(strs));
    }

    static List<List<String>> groupAnagrams(String[] strs) {

        if (strs.length == 0) {
            return new ArrayList();
        }

        List<List<String>> groups = new ArrayList<>();

        HashSet<String> isInGroup = new HashSet<>();

        for (int i = 0; i < strs.length; i++) {

            if (!isInGroup.contains(strs[i])) {
                List<String> singleGroup = new ArrayList<>();
                singleGroup.add(strs[i]);
                isInGroup.add(strs[i]);

                if (i == strs.length - 1) {
                    groups.add(singleGroup);
                }

                for (int j = i; j < strs.length - 1; j++) {
                    if (!isInGroup.contains(strs[j + 1]) && areAnagram(strs[i], strs[j + 1])) {
                        singleGroup.add(strs[j + 1]);
                        isInGroup.add(strs[j + 1]);
                    }

                    if(j == strs.length - 2) {
                        groups.add(singleGroup);
                    }
                }
            }
        }

        return groups;
    }

    static boolean areAnagram(String s1, String s2)
    {
        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();

        int n1 = str1.length;
        int n2 = str2.length;

        if (n1 != n2) {
            return false;
        }

        Arrays.sort(str1);
        Arrays.sort(str2);

        for (int i = 0; i < n1; i++) {
            if (str1[i] != str2[i]) {
                return false;
            }
        }

        return true;
    }
}
